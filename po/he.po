# Hebrew translation for command-not-found
# Copyright (c) 2007 Rosetta Contributors and Canonical Ltd 2007
# This file is distributed under the same license as the command-not-found package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: command-not-found\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-20 14:38+0100\n"
"PO-Revision-Date: 2022-07-12 14:38+0000\n"
"Last-Translator: Yaron <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew <he@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2023-01-20 13:36+0000\n"
"X-Generator: Launchpad (build e04feada4865d7ca041d8edeee137fe87b5bcb3e)\n"

#: ../CommandNotFound/CommandNotFound.py:160
msgid "Do you want to install it? (N/y)"
msgstr "להתקין אותה? (N/y)"

#: ../CommandNotFound/CommandNotFound.py:169
msgid "y"
msgstr "y"

#: ../CommandNotFound/CommandNotFound.py:181
#, python-format
msgid "Command '%s' not found, but there are %s similar ones."
msgstr "הפקודה ‚%s’ לא נמצאה אך יש %s דומות לה."

#: ../CommandNotFound/CommandNotFound.py:185
#: ../CommandNotFound/CommandNotFound.py:343
#, python-format
msgid "Command '%s' not found, did you mean:"
msgstr "הפקודה ‚%s’ לא נמצאה, יכול להיות שהתכוונת:"

#: ../CommandNotFound/CommandNotFound.py:191
#, python-format
msgid "  command '%s' from snap %s%s"
msgstr "  לפקודה ‚%s’ מה־snap‏ %s%s"

#: ../CommandNotFound/CommandNotFound.py:197
#: ../CommandNotFound/CommandNotFound.py:344
#: ../CommandNotFound/CommandNotFound.py:345
#, python-format
msgid "  command '%s' from deb %s%s"
msgstr "  לפקודה ‚%s’ מה־deb‏ %s%s"

#: ../CommandNotFound/CommandNotFound.py:199
#: ../CommandNotFound/CommandNotFound.py:272
#: ../CommandNotFound/CommandNotFound.py:294
msgid "See 'snap info <snapname>' for additional versions."
msgstr "ניתן להריץ את ‚snap info <snapname>‎’ להצגת גרסאות נוספות."

#: ../CommandNotFound/CommandNotFound.py:202
#: ../CommandNotFound/CommandNotFound.py:204
#, python-format
msgid "Try: %s <deb name>"
msgstr "כדאי לנסות: ‎%s <deb name>‎"

#: ../CommandNotFound/CommandNotFound.py:208
#, python-format
msgid "Command '%(command)s' not found, but can be installed with:"
msgstr "הפקודה ‚%(command)s’ לא נמצאה, אך ניתן להתקין אותה עם:"

#: ../CommandNotFound/CommandNotFound.py:220
#: ../CommandNotFound/CommandNotFound.py:233
msgid "Please ask your administrator."
msgstr "נא לבקש מהנהלת המערכת שלך."

#: ../CommandNotFound/CommandNotFound.py:235
#, python-format
msgid "You will have to enable the component called '%s'"
msgstr "יש להפעיל את הרכיב שנקרא ‚%s’"

#: ../CommandNotFound/CommandNotFound.py:256
#, python-format
msgid "You will have to enable component called '%s'"
msgstr "יש להפעיל רכיב שנקרא ‚%s’"

#: ../CommandNotFound/CommandNotFound.py:258
msgid "Ask your administrator to install one of them."
msgstr "נא לבקש מההנהלה שלך להתקין אחד מהם."

#: ../CommandNotFound/CommandNotFound.py:292
#, python-format
msgid "See 'snap info %s' for additional versions."
msgstr "יש להריץ את ‚snap info %s’ לקבלת גרסאות נוספות."

#: ../CommandNotFound/CommandNotFound.py:318
#, python-format
msgid "Command '%(command)s' is available in '%(place)s'"
msgstr "הפקודה ‚%(command)s’ זמינה תחת ‚%(place)s’"

#: ../CommandNotFound/CommandNotFound.py:320
#, python-format
msgid "Command '%(command)s' is available in the following places"
msgstr "הפקודה ‚%(command)s’ זמינה במקומות הבאים"

#: ../CommandNotFound/CommandNotFound.py:325
#, python-format
msgid ""
"The command could not be located because '%s' is not included in the PATH "
"environment variable."
msgstr "לא ניתן לאתר את הפקודה כיוון ש־‚%s’ לא נכלל במשתנה הסביבתי PATH."

#: ../CommandNotFound/CommandNotFound.py:327
msgid ""
"This is most likely caused by the lack of administrative privileges "
"associated with your user account."
msgstr "זה נקרא כנראה עקב מחסור בהרשאות ניהוליות שמקושרות לחשבון המשתמש שלך."

#: ../command-not-found:77
#, c-format
msgid "%prog [options] <command-name>"
msgstr "%prog [options] <command-name>"

#: ../command-not-found:80
msgid "use this path to locate data fields"
msgstr "יש להשתמש בנתיב הזה כדי לאתר שדות נתונים"

#: ../command-not-found:83
msgid "ignore local binaries and display the available packages"
msgstr "להתעלם מקבצים בינריים מקומיים ולהציג את החבילות הזמינות"

#: ../command-not-found:86
msgid "don't print '<command-name>: command not found'"
msgstr "לא להציג ‚‎<command-name>: command not found’"

#: ../command-not-found:92
msgid ""
"Could not find command-not-found database. Run 'sudo apt update' to populate "
"it."
msgstr ""
"לא ניתן למצוא את מסד נתונים של command-not-found. יש להריץ ‚sudo apt update’ "
"כדי לאכלס אותו."

#: ../command-not-found:93 ../command-not-found:96
#, c-format
msgid "%s: command not found"
msgstr "%s: הפקודה לא נמצאה"

#: ../CommandNotFound/util.py:25
msgid "Sorry, command-not-found has crashed! Please file a bug report at:"
msgstr "command-not-found קרסה! עמך הסליחה, נא לדווח על תקלה תחת"

#: ../CommandNotFound/util.py:27
msgid "Please include the following information with the report:"
msgstr "נא לכלול את המידע הבא בדיווח:"

#: ../CommandNotFound/util.py:29
#, python-format
msgid "command-not-found version: %s"
msgstr "גרסת command-not-found‏: %s"

#: ../CommandNotFound/util.py:30
#, python-format
msgid "Python version: %d.%d.%d %s %d"
msgstr "גרסת Python‏: %d.%d.%d %s %d"

#: ../CommandNotFound/util.py:36
msgid "Exception information:"
msgstr "פרטי חריגה:"
